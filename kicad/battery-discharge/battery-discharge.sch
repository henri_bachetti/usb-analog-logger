EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:battery-discharge-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L usb-analog-logger U1
U 1 1 5D570B1E
P 5350 2750
F 0 "U1" H 4600 2300 60  0000 C CNN
F 1 "usb-analog-logger" H 4600 2200 60  0000 C CNN
F 2 "" H 5350 2750 60  0000 C CNN
F 3 "" H 5350 2750 60  0000 C CNN
	1    5350 2750
	1    0    0    -1  
$EndComp
$Comp
L Battery BT1
U 1 1 5D570B55
P 6600 3000
F 0 "BT1" H 6700 3050 50  0000 L CNN
F 1 "3.6V" H 6700 2950 50  0000 L CNN
F 2 "" V 6600 3040 50  0000 C CNN
F 3 "" V 6600 3040 50  0000 C CNN
	1    6600 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6600 2850 6600 2750
Wire Wire Line
	5550 2750 6600 2750
Wire Wire Line
	6600 2750 6800 2750
Wire Wire Line
	6600 3150 6600 3250
Wire Wire Line
	6600 3250 6600 3350
Wire Wire Line
	6600 3250 6400 3250
Wire Wire Line
	6400 3250 6050 3250
Wire Wire Line
	6050 3250 6050 3150
Wire Wire Line
	6050 3150 5550 3150
Wire Wire Line
	5550 3950 6800 3950
Wire Wire Line
	6800 2750 6800 3950
Wire Wire Line
	6800 3950 6800 4350
Connection ~ 6600 2750
Wire Wire Line
	6800 4350 5550 4350
Connection ~ 6800 3950
$Comp
L R R2
U 1 1 5D570C12
P 6600 3500
F 0 "R2" V 6680 3500 50  0000 C CNN
F 1 "82" V 6600 3500 50  0000 C CNN
F 2 "" V 6530 3500 50  0000 C CNN
F 3 "" H 6600 3500 50  0000 C CNN
	1    6600 3500
	1    0    0    -1  
$EndComp
Connection ~ 6600 3250
$Comp
L R R1
U 1 1 5D570C49
P 6400 3500
F 0 "R1" V 6480 3500 50  0000 C CNN
F 1 "22" V 6400 3500 50  0000 C CNN
F 2 "" V 6330 3500 50  0000 C CNN
F 3 "" H 6400 3500 50  0000 C CNN
	1    6400 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3350 6400 3250
Connection ~ 6400 3250
Wire Wire Line
	6400 3650 6400 4150
Wire Wire Line
	6600 4550 6600 3650
Wire Wire Line
	6400 4150 5550 4150
Wire Wire Line
	5550 4550 6600 4550
$EndSCHEMATC
