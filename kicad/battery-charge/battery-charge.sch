EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-misc-modules
LIBS:battery-charge-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L usb-analog-logger U1
U 1 1 5D570B1E
P 5350 2850
F 0 "U1" H 4600 2400 60  0000 C CNN
F 1 "usb-analog-logger" H 4600 2300 60  0000 C CNN
F 2 "" H 5350 2850 60  0000 C CNN
F 3 "" H 5350 2850 60  0000 C CNN
	1    5350 2850
	1    0    0    -1  
$EndComp
$Comp
L Battery BT1
U 1 1 5D570B55
P 6200 3000
F 0 "BT1" H 6300 3050 50  0000 L CNN
F 1 "3.6V" H 6300 2950 50  0000 L CNN
F 2 "" V 6200 3040 50  0000 C CNN
F 3 "" V 6200 3040 50  0000 C CNN
	1    6200 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6200 2850 6200 2750
Wire Wire Line
	6200 2750 6800 2750
Wire Wire Line
	6200 3250 6200 3150
Wire Wire Line
	5550 3250 6800 3250
$Comp
L nimh-battery-charger U?
U 1 1 5D5CE505
P 8150 2750
F 0 "U?" H 8700 2350 60  0000 C CNN
F 1 "nimh-battery-charger" H 8700 2500 60  0000 C CNN
F 2 "" H 9100 2200 60  0000 C CNN
F 3 "" H 9100 2200 60  0000 C CNN
	1    8150 2750
	-1   0    0    -1  
$EndComp
Connection ~ 6200 3250
Wire Wire Line
	6800 3450 5850 3450
Wire Wire Line
	5850 3450 5850 2850
Wire Wire Line
	5850 2850 5550 2850
Wire Wire Line
	5550 3150 5800 3150
Wire Wire Line
	5800 3150 5800 3550
Wire Wire Line
	5800 3550 6800 3550
Wire Wire Line
	6800 3650 5750 3650
Wire Wire Line
	5750 3650 5750 2600
Wire Wire Line
	5750 2600 3500 2600
Wire Wire Line
	3500 2600 3500 3150
Wire Wire Line
	3500 3150 3600 3150
$EndSCHEMATC
