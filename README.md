# USB ANALOG & LOGICAL LOGGER

The purpose of this page is to explain step by step the realization of an USB analog and digital logger based on ARDUINO.

It can measure voltages, currents and logic signals.

This logger can transmit the measurements to a PYTHON software on a PC to draw a graph.
It's possible to use the Processing too, or any langage able to draw graphs.

The board uses the following components :

 * an ARDUINO UNO
 * an ADS1115
 * 3 PN2222
 * a buzzer
 * a LED
 * some passive components
 * the board is powered by the USB.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2019/08/un-logger-analogique-et-digital.html

