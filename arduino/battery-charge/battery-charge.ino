
#include <Bounce2.h>
#include <ADS1115.h>

#define PERIOD                2000
#define BUZZER_FREQ           1800

#define BUTTON_START          2
#define BUTTON_STOP           3
#define ALERT_RDY_PIN         4
#define CHARGER_LED           5
#define BUZZER                12
#define LED                   13

#define STATE_IDLE            0
#define STATE_CHARGE          1
#define STATE_FINISHED        2

#define SHUNT                 0.8

//#define ADS1115_SERIAL_DEBUG

int state = STATE_IDLE;

Bounce startButton = Bounce();
Bounce stopButton = Bounce();
ADS1115 adc0(ADS1115_DEFAULT_ADDRESS);
void pollAlertReadyPin()
{
  for (uint32_t i = 0; i < 100000; i++)
    if (!digitalRead(ALERT_RDY_PIN)) return;
  Serial.println("Failed to wait for AlertReadyPin, it's stuck high!");
}

float getVoltage(void)
{
  adc0.setMultiplexer(ADS1115_MUX_P0_NG);
  adc0.triggerConversion();
  pollAlertReadyPin();
  return adc0.getMilliVolts(false);
}

float getShuntVoltage(void)
{
  adc0.setMultiplexer(ADS1115_MUX_P1_NG);
  adc0.triggerConversion();
  pollAlertReadyPin();
  return adc0.getMilliVolts(false);
}

void beep(int time)
{
  tone(BUZZER, BUZZER_FREQ);
  delay(time);
  noTone(BUZZER);
}

void flash(void)
{
  digitalWrite(LED, LOW);
  delay(50);
  digitalWrite(LED, HIGH);
}

void mdelay(int ms)
{
  unsigned long start = millis();
  for (;;) {
    stopButton.update();
    if (stopButton.read() == 0) {
      return;
    }
    unsigned long now = millis();
    unsigned long elapsed = now - start;
    if (elapsed >= ms)
      return;
  }
}

void setup() {
  Wire.begin();
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, HIGH);
  pinMode(BUTTON_START, INPUT_PULLUP);
  pinMode(BUTTON_STOP, INPUT_PULLUP);
  pinMode(CHARGER_LED, INPUT);
  pinMode(ALERT_RDY_PIN, INPUT_PULLUP);
  startButton.attach(BUTTON_START);
  startButton.interval(5);
  stopButton.attach(BUTTON_STOP);
  stopButton.interval(5);
  Serial.println("Testing device connections...");
  Serial.println(adc0.testConnection() ? "ADS1115 connection successful" : "ADS1115 connection failed");
  adc0.initialize();
  adc0.setMode(ADS1115_MODE_SINGLESHOT);
  adc0.setRate(ADS1115_RATE_8);
  adc0.setGain(ADS1115_PGA_6P144);
  adc0.setConversionReadyPinMode();
#ifdef ADS1115_SERIAL_DEBUG
  adc0.showConfigRegister();
  Serial.print("HighThreshold="); Serial.println(adc0.getHighThreshold(), BIN);
  Serial.print("LowThreshold="); Serial.println(adc0.getLowThreshold(), BIN);
#endif
}

void sendVoltageCurrent(float voltage, float current)
{
  Serial.print(millis() / 1000); Serial.print(":VADS0"); Serial.print("="); Serial.print(voltage); Serial.print(";IADS1"); Serial.print("="); Serial.println(current);
  flash();
}

unsigned long start;

void loop()
{
  float voltage, current, shuntVoltage;

  startButton.update();
  stopButton.update();
  switch (state) {
    case STATE_IDLE:
      if (startButton.read() == 0) {
        Serial.println("title=Cellevia ACCU-600 NIMH Battery Charge Curve");
        start = millis();
        beep(100);
        state = STATE_CHARGE;
      }
      break;
    case STATE_CHARGE:
      if (stopButton.read() == 0) {
        Serial.println("STOP");
        state = STATE_IDLE;
        beep(100);
      }
      if (digitalRead(CHARGER_LED) == HIGH) {
        Serial.println("STOP");
        state = STATE_FINISHED;
      }
      if (millis() % PERIOD == 0) {
        voltage = getVoltage();
        shuntVoltage = getShuntVoltage();
        current = shuntVoltage / SHUNT;
        voltage -= shuntVoltage;
        sendVoltageCurrent(voltage, current);
      }
      break;
    case STATE_FINISHED:
      if (stopButton.read() == 0) {
        state = STATE_IDLE;
      }
      beep(100);
      mdelay(1000);
      break;
  }
}

