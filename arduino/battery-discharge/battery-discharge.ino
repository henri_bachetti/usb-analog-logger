
#include <Bounce2.h>
#include <ADS1115.h>

#define PERIOD                2000
#define BUZZER_FREQ           1800

#define BUTTON_START          2
#define BUTTON_STOP           3
#define ALERT_RDY_PIN         4
#define RELAY_HIGH            10
#define RELAY_LOW             11
#define BUZZER                12
#define LED                   13

#define STATE_IDLE            0
#define STATE_DISCHARGE_HIGH  1
#define STATE_DISCHARGE_LOW   2
#define STATE_FINISHED        3

//#define ADS1115_SERIAL_DEBUG

int state = STATE_IDLE;

Bounce startButton = Bounce();
Bounce stopButton = Bounce();
ADS1115 adc0(ADS1115_DEFAULT_ADDRESS);

void pollAlertReadyPin()
{
  for (uint32_t i = 0; i < 100000; i++)
    if (!digitalRead(ALERT_RDY_PIN)) return;
  Serial.println("Failed to wait for AlertReadyPin, it's stuck high!");
}

float getBatteryVoltage(void)
{
  adc0.setMultiplexer(ADS1115_MUX_P0_NG);
  adc0.triggerConversion();
  pollAlertReadyPin();
  return adc0.getMilliVolts(false);
}

void beep(int time)
{
  tone(BUZZER, BUZZER_FREQ);
  delay(time);
  noTone(BUZZER);
}

void flash(void)
{
  digitalWrite(LED, LOW);
  delay(50);
  digitalWrite(LED, HIGH);
}

void mdelay(int ms)
{
  unsigned long start = millis();
  for (;;) {
    stopButton.update();
    if (stopButton.read() == 0) {
      return;
    }
    unsigned long now = millis();
    unsigned long elapsed = now - start;
    if (elapsed >= ms)
      return;
  }
}

void setup() {
  Wire.begin();
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  pinMode(RELAY_HIGH, OUTPUT);
  pinMode(RELAY_LOW, OUTPUT);
  pinMode(BUTTON_START, INPUT_PULLUP);
  pinMode(ALERT_RDY_PIN, INPUT_PULLUP);
  digitalWrite(LED, HIGH);
  startButton.attach(BUTTON_START);
  startButton.interval(5);
  pinMode(BUTTON_STOP, INPUT_PULLUP);
  stopButton.attach(BUTTON_STOP);
  stopButton.interval(5);
  Serial.println("Testing device connections...");
  Serial.println(adc0.testConnection() ? "ADS1115 connection successful" : "ADS1115 connection failed");
  adc0.initialize();
  adc0.setMode(ADS1115_MODE_SINGLESHOT);
  adc0.setRate(ADS1115_RATE_8);
  adc0.setGain(ADS1115_PGA_6P144);
  adc0.setConversionReadyPinMode();
#ifdef ADS1115_SERIAL_DEBUG
  adc0.showConfigRegister();
  Serial.print("HighThreshold="); Serial.println(adc0.getHighThreshold(), BIN);
  Serial.print("LowThreshold="); Serial.println(adc0.getLowThreshold(), BIN);
#endif
}

void loop()
{
  float voltage;

  startButton.update();
  stopButton.update();
  switch (state) {
    case STATE_IDLE:
      if (startButton.read() == 0) {
        Serial.println("title=Cellevia ACCU-600 NIMH Battery Discharge Curve");
        beep(100);
        digitalWrite(RELAY_HIGH, HIGH);
        state = STATE_DISCHARGE_HIGH;
      }
      break;
    case STATE_DISCHARGE_HIGH:
      if (stopButton.read() == 0) {
        Serial.println("STOP");
        digitalWrite(RELAY_HIGH, LOW);
        state = STATE_IDLE;
        beep(100);
      }
      voltage = getBatteryVoltage();
      Serial.print(millis() / 1000); Serial.print(":VADS0"); Serial.print("="); Serial.println(voltage);
      if (voltage < 3000.0) {
        digitalWrite(RELAY_HIGH, LOW);
        digitalWrite(RELAY_LOW, HIGH);
        state = STATE_DISCHARGE_LOW;
      }
      flash();
      mdelay(PERIOD);
      break;
    case STATE_DISCHARGE_LOW:
      if (stopButton.read() == 0) {
        Serial.println("STOP");
        digitalWrite(RELAY_LOW, LOW);
        state = STATE_IDLE;
        beep(100);
      }
      voltage = getBatteryVoltage();
      Serial.print(millis() / 1000); Serial.print(":VADS0"); Serial.print("="); Serial.println(voltage);
      if (voltage < 3000.0) {
        digitalWrite(RELAY_LOW, LOW);
        Serial.println("STOP");
        state = STATE_FINISHED;
      }
      flash();
      mdelay(PERIOD);
      break;
    case STATE_FINISHED:
      if (stopButton.read() == 0) {
        state = STATE_IDLE;
      }
      beep(100);
      mdelay(1000);
      break;
  }
}

