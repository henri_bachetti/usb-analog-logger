#!/usr/bin/env python
# -*- coding: utf-8 -*-

#
# A PYTHON script to display data from the analog logger bench
# Usage : python log-voltage-current.py usb_device <csvfile>
#
 
import sys, time
import serial
import matplotlib.pyplot as plt; plt.rcdefaults()
import seaborn as sns

sns.set()
duration = 0
plot = "plot"
voltage_Data = {}
voltage = 0
maxVoltage = 0.0
maxCurrent = 0.0
current_Data = {}
current = 0
x_ticks = []

if len(sys.argv) < 2:
	print "ERROR: USB device name is missing"
	print "Usage: python %s usb_device <csvfile>" % sys.argv[0]
	sys.exit(1)

# default log filename
logFileName = 'log-voltage-current.csv'

if len(sys.argv) > 2:
	logFileName = sys.argv[2]

print "log file:", logFileName
logFile = open(logFileName, 'w+')
title = 'TITLE'
start_time = time.time()
line = serial.Serial(sys.argv[1], 115200, timeout=1000, parity=serial.PARITY_NONE)
while 1:
	s = line.readline()
	if s.startswith("title"):
		name, title = s.split('=')
		print title
		title = title.strip('\r\n')
	elif s.startswith("STOP"):
		break
	else:
#
# Accepts two forms of data lines :
# 2:VADS0=3277.31
#   2: the time in seconds
#   VADS0 : the voltage ADC channel
#   3277.31 : the voltage in mV
# 5:VADS0=3277.12;IADS1=40.13
#   5: the time in seconds
#   VADS0 : the voltage ADC channel
#   3277.12 : the voltage in mV
#   IADS1 : the current ADC channel
#   40.13 : the current in mA
#
		if s.find(';') != -1:            # voltage + current
			u, i = s.split(';')
			t, d = u.split(':')
			adc1, voltage = d.split('=')
			voltage = float(voltage) / 1000
			t = int(t)
			x_ticks.append(t)
			voltage_Data[t] = voltage
			print t, adc1, '=', voltage_Data[t]
			if maxVoltage < voltage:
				maxVoltage = voltage
			adc2, current = i.split('=')
			current = float(current) / 1000
			current_Data[t] = current
			if maxCurrent < current:
				maxCurrent = current
			print t, adc2, '=', current_Data[t]
		elif s.find(':') != -1:          # voltage only
			t, d = s.split(':')
			adc1, voltage = d.split('=')
			voltage = float(voltage) / 1000
			t = int(t)
			x_ticks.append(t)
			voltage_Data[t] = voltage
			print t, adc1, '=', voltage_Data[t]
			if maxVoltage < voltage:
				maxVoltage = voltage
stop_time = time.time()
duration = stop_time - start_time
print 'duration=', duration
voltageSerie = []
currentSerie = []
logFile.write(title + '\n')

#
# Build the series
#

for t in range(int(duration)):
	if t in voltage_Data:
		voltage = voltage_Data[t]
		voltageSerie.append(voltage)
		logFile.write('%s;%s' % (t, voltage))
		if not t in current_Data:
			logFile.write('\n')
		else:
			current = current_Data[t]
			currentSerie.append(current)
			logFile.write(';' + str(current) + '\n')
logFile.close()

#print maxVoltage, x_ticks, voltageSerie

voltageLine, = plt.plot(x_ticks, voltageSerie, label='Voltage')
if len(currentSerie):
	currrentLine, = plt.plot(x_ticks, currentSerie, label='Current')
	plt.legend(handles=[voltageLine, currrentLine])
else:
	plt.legend(handles=[voltageLine, ])
plt.xlabel('Seconds')
plt.yscale('linear')
plt.ylabel('Volts or Amps')
plt.grid(True,which="both")
if len(currentSerie):
	plt.ylim(ymin=0, ymax=max(maxVoltage, maxCurrent)+1)
else :
	plt.ylim(ymin=0, ymax=maxVoltage+1)
plt.title(title)
plt.show()

